# FadingActionBar

#### 项目介绍

滑动抬头栏可以渐隐,提供组件可以拼接


#### 安装教程

方式一：
 1. 下载模块代码添加到自己的工程
    
 2. 关联使用
 ```
 dependencies {
         implementation project(":library")
 ……
 }
 ```
 3. 在系统的settings.gradle中添加
 ```
 include ':library'
 ```


- 方式二：

```
allprojects {
    repositories {
        mavenCentral()        
    }
}

...
dependencies {
    ...
    implementation 'com.gitee.archermind-ti:FadingActionBar:1.0.0-beta'
    ...
```

#### 使用说明

```java
   示例:
   FadingActionBarHelper helper = new FadingActionBarHelper()
                .actionBarBackground(Color.getIntColor("#ff8800")) //抬头栏渐隐颜色
                .headerLayout(ResourceTable.Layout_header_light) //抬头标题布局(sample中用图片演示)
                .contentLayout(ResourceTable.Layout_ability_scrollview); //内容布局
        setUIContent(helper.createView(this)); //使用creatView()方法拼接得到ComponentContainer对象加载

```

#### 效果演示

 ![](show.png)
 ![](1.gif)

#### 版本迭代

- v1.0.0


#### 版权和许可信息

在 [Apache 2.0 License](LICENSE)下获得许可

