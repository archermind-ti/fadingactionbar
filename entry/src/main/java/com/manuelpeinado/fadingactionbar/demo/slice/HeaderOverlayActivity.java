package com.manuelpeinado.fadingactionbar.demo.slice;

import com.manuelpeinado.fadingactionbar.demo.ResourceTable;
import com.manuelpeinado.fadingactionbar.library.FadingActionBarHelper;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class HeaderOverlayActivity extends AbilitySlice {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        FadingActionBarHelper helper = new FadingActionBarHelper()
                .actionBarBackground(Color.getIntColor("#ff8800"))
                .headerLayout(ResourceTable.Layout_header_overlay)
                .contentLayout(ResourceTable.Layout_ability_listview);
        helper.setScreenWidth(screenWidth(this));
        setUIContent(helper.createView(this));
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainerCountries);
        ArrayList<String> items = loadItems(ResourceTable.Media_nyc_sites);
        CountriesProvider countriesProvider = new CountriesProvider(items, getContext());
        listContainer.setItemProvider(countriesProvider);
    }

    private ArrayList<String> loadItems(int rawResourceId) {
        try {
            ArrayList<String> countries = new ArrayList<String>();
            InputStream inputStream = getResourceManager().getResource(rawResourceId);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                countries.add(line);
            }
            reader.close();
            return countries;
        } catch (IOException | NotExistException e) {
            return null;
        }
    }

    public int screenWidth(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(point);
        return point.getPointXToInt();
    }
}
