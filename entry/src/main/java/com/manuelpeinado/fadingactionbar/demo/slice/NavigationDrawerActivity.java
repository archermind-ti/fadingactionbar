package com.manuelpeinado.fadingactionbar.demo.slice;

import com.manuelpeinado.fadingactionbar.demo.ResourceTable;
import com.manuelpeinado.fadingactionbar.library.FadingActionBarHelper;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;

public class NavigationDrawerActivity extends AbilitySlice {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        FadingActionBarHelper helper = new FadingActionBarHelper()
                .actionBarBackground(Color.getIntColor("#ff8800"))
                .headerLayout(ResourceTable.Layout_header)
                .contentLayout(ResourceTable.Layout_ability_scrollview);
        helper.setDrawer(true);
        helper.setMenu(false);

        ArrayList<city> cities = new ArrayList<>();
        cities.add(new city(getContext().getString(ResourceTable.String_new_york_city), ResourceTable.Media_ny));
        cities.add(new city("Tokyo", ResourceTable.Media_tokyo));
        cities.add(new city("London", ResourceTable.Media_london));
        CitiesProvider citiesProvider = new CitiesProvider(cities, this);
//        ListContainer.ItemClickedListener itemClickedListener = (listContainer, component, i, l) -> {
//            Image image = (Image) findComponentById(ResourceTable.Id_hearImage);
//            image.setPixelMap(cities.get(i).getResID());
//            helper.closeDrawer();
//        };
        helper.setProvider(citiesProvider);
        helper.setItemClickedListener(position -> {
            Image image = (Image) findComponentById(ResourceTable.Id_hearImage);
            image.setPixelMap(cities.get(position).getResID());
            helper.closeDrawer();
        });

        setUIContent(helper.createView(this));
    }
}


class city {
    private String cityName;
    private int resID;

    public city(String cityName, int resID) {
        this.cityName = cityName;
        this.resID = resID;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getResID() {
        return resID;
    }

    public void setResID(int resID) {
        this.resID = resID;
    }
}

class CitiesProvider extends BaseItemProvider {

    private ArrayList<city> list;

    private Context context;

    public CitiesProvider(ArrayList<city> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public ArrayList<city> getList() {
        return list;
    }

    public void setList(ArrayList<city> list) {
        this.list = list;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_city, null, false);
        } else {
            cpt = convertComponent;
        }
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_item_city);
        text.setText(list.get(position).getCityName());
        text.setTextSize(50);
        return cpt;
    }


}
