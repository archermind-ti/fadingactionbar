package com.manuelpeinado.fadingactionbar.demo.slice;

import ohos.aafwk.ability.AbilitySlice;

public class ActivityInfo {
    public Class<? extends AbilitySlice> activityClass;
    public int titleResourceId;


    public ActivityInfo(Class<? extends AbilitySlice> activityClass, int titleResourceId) {
        this.activityClass = activityClass;
        this.titleResourceId = titleResourceId;
    }
}
