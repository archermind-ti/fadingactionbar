package com.manuelpeinado.fadingactionbar.demo.slice;

import com.manuelpeinado.fadingactionbar.demo.ResourceTable;
import com.manuelpeinado.fadingactionbar.library.FadingActionBarHelper;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;

import java.io.*;
import java.util.ArrayList;

public class ListViewActivity extends AbilitySlice {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        FadingActionBarHelper helper = new FadingActionBarHelper()
                .actionBarBackground(Color.getIntColor("#ff8800"))
                .headerLayout(ResourceTable.Layout_header)
                .contentLayout(ResourceTable.Layout_ability_listview);
        helper.setScreenWidth(screenWidth(this));
        setUIContent(helper.createView(this));
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainerCountries);
        ArrayList<String> items = loadItems(ResourceTable.Media_nyc_sites);
        CountriesProvider countriesProvider = new CountriesProvider(items, getContext());
        listContainer.setItemProvider(countriesProvider);
    }

    private ArrayList<String> loadItems(int rawResourceId) {
        try {
            ArrayList<String> countries = new ArrayList<String>();
            InputStream inputStream = getResourceManager().getResource(rawResourceId);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                countries.add(line);
            }
            reader.close();
            return countries;
        } catch (IOException | NotExistException e) {
            return null;
        }
    }

    public int screenWidth(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(point);
        return point.getPointXToInt();
    }
}

class CountriesProvider extends BaseItemProvider {

    private ArrayList<String> list;

    private Context context;

    public CountriesProvider(ArrayList<String> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public CountriesProvider(ArrayList<String> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_drawer_list_item, null, false);
        } else {
            cpt = convertComponent;
        }
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_item_index);
        text.setText(list.get(position));
        text.setTextSize(40);
        return cpt;
    }


}
