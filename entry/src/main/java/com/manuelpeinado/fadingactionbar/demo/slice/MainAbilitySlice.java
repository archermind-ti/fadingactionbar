package com.manuelpeinado.fadingactionbar.demo.slice;

import com.manuelpeinado.fadingactionbar.demo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.Arrays;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    private List<ActivityInfo> activitiesInfo = Arrays.asList(
            new ActivityInfo(ScrollViewActivity.class, ResourceTable.String_activity_title_scrollview),
            new ActivityInfo(ListViewActivity.class, ResourceTable.String_activity_title_listview),
            new ActivityInfo(LightBackgroundActivity.class, ResourceTable.String_activity_title_light_bg),
            new ActivityInfo(LightActionBarActivity.class, ResourceTable.String_activity_title_light_ab),
            new ActivityInfo(SampleFragmentActivity.class, ResourceTable.String_activity_title_fragment),
            new ActivityInfo(NoParallaxActivity.class, ResourceTable.String_activity_title_no_parallax),
            new ActivityInfo(NavigationDrawerActivity.class, ResourceTable.String_activity_title_navigation),
            new ActivityInfo(HeaderOverlayActivity.class, ResourceTable.String_activity_title_header_overlay),
            new ActivityInfo(WebViewActivity.class, ResourceTable.String_activity_title_webview),
            new ActivityInfo(ShortContentActivity.class, ResourceTable.String_activity_title_short_content));

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        init();
    }

    @Override
    public void init() {

        ActivityProvider activityProvider = new ActivityProvider(activitiesInfo, getContext());
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list);
        listContainer.setItemProvider(activityProvider);
        listContainer.setItemClickedListener((listContainer1, component, i, l) -> {
            Class<? extends AbilitySlice> class_ = activitiesInfo.get(i).activityClass;
            try {
                present(class_.newInstance(), new Intent());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}


class ActivityProvider extends BaseItemProvider {

    private List<ActivityInfo> list;

    private Context context;

    public ActivityProvider(List<ActivityInfo> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public ActivityProvider(List<ActivityInfo> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_drawer_list_item, null, false);
        } else {
            cpt = convertComponent;
        }
        int resId = list.get(position).titleResourceId;
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_item_index);
        text.setText(context.getString(resId));
        return cpt;
    }


}
