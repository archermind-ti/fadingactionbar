package com.manuelpeinado.fadingactionbar.demo.slice;

import com.manuelpeinado.fadingactionbar.demo.ResourceTable;
import com.manuelpeinado.fadingactionbar.library.FadingActionBarHelper;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;

public class LightActionBarActivity extends AbilitySlice {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        FadingActionBarHelper helper = new FadingActionBarHelper()
                .actionBarBackground(Color.getIntColor("#00EE00"))
                .headerLayout(ResourceTable.Layout_header_light)
                .contentLayout(ResourceTable.Layout_ability_scrollview);
        setUIContent(helper.createView(this));
    }

}
