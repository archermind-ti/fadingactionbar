package com.manuelpeinado.fadingactionbar.demo.slice;

import com.manuelpeinado.fadingactionbar.demo.ResourceTable;
import com.manuelpeinado.fadingactionbar.library.FadingActionBarHelper;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;


public class ShortContentActivity extends AbilitySlice {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        FadingActionBarHelper helper = new FadingActionBarHelper()
                .actionBarBackground(Color.getIntColor("#ff8800"))
                .headerLayout(ResourceTable.Layout_header)
                .contentLayout(ResourceTable.Layout_ability_short_content);
        helper.setScreenWidth(screenWidth(this));
        helper.setScreenHeight(screenHeight(this)-675);
        setUIContent(helper.createView(this));
    }

    public int screenWidth(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(point);
        return point.getPointXToInt();
    }

    public int screenHeight(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(point);
        return point.getPointYToInt();
    }
}
