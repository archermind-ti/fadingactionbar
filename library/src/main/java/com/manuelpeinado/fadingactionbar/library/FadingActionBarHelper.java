package com.manuelpeinado.fadingactionbar.library;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.List;


public class FadingActionBarHelper {
    private int actionBarColor = Color.BLUE.getValue();
    private int headerResId = -1;
    private int contentId = -1;
    private int screenWidth = -1;
    //屏幕宽度
    private int mScreenWith;
    private int screenHeight = -1;
    private boolean isMenu = true;
    private boolean isDrawer = false;
    private String textBar = "New York City";
    private Component drawer;
    private Component drawerBackground;
    private boolean running;
    private boolean drawerOpen;
    private int background_alpha = 0x88;
    private BaseItemProvider provider = null;
    private OnDrawerItemClickListener mOnDrawerItemClickListener = null;
    private Component header = null;

    public int getScreenHeight() {
        return screenHeight;
    }

    public void setScreenHeight(int screenHeight) {
        this.screenHeight = screenHeight;
    }

    public OnDrawerItemClickListener getItemClickedListener() {
        return mOnDrawerItemClickListener;
    }

    public void setItemClickedListener(OnDrawerItemClickListener itemClickedListener) {
        mOnDrawerItemClickListener = itemClickedListener;
    }

    public BaseItemProvider getProvider() {
        return provider;
    }

    public void setProvider(BaseItemProvider provider) {
        this.provider = provider;
    }

    public boolean isDrawer() {
        return isDrawer;
    }

    public void setDrawer(boolean drawer) {
        isDrawer = drawer;
    }

    public String getTextBar() {
        return textBar;
    }

    public void setTextBar(String textBar) {
        this.textBar = textBar;
    }

    public boolean isMenu() {
        return isMenu;
    }

    public void setMenu(boolean light) {
        isMenu = light;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(int listContainer) {
        screenWidth = listContainer;
    }

    public FadingActionBarHelper actionBarBackground(int color) {
        actionBarColor = color;
        return this;
    }

    public FadingActionBarHelper headerLayout(int resId) {
        headerResId = resId;
        return this;
    }

    public FadingActionBarHelper contentLayout(int resId) {
        contentId = resId;
        return this;
    }

    public ComponentContainer createView(LayoutScatter layoutScatter) {
        if (layoutScatter == null) {
            return null;
        }
        ComponentContainer component = (ComponentContainer) layoutScatter.parse(ResourceTable.Layout_layout_helper, null, false);
        return createView(component);
    }

    public ComponentContainer createView(Context context) {
        if (context == null) {
            return null;
        }
        ComponentContainer component = (ComponentContainer) LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_helper, null, false);
        initDrawer(component);
        return createView(component);
    }

    public void openDrawer() {
        if (!drawerOpen) {
            startAnimation();
        }
    }

    public void closeDrawer() {
        if (drawerOpen) {
            startAnimation();
        }
    }


    private void initDrawer(ComponentContainer component) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(component.getContext()).get().getSize(point);
        mScreenWith = point.getPointYToInt();
        //initDrawer
        drawer = component.findComponentById(ResourceTable.Id_drawerLayout);
        drawerBackground = component.findComponentById(ResourceTable.Id_drawerBackground);
        ShapeElement element = new ShapeElement();
        element.setAlpha(background_alpha);
        element.setRgbColor(new RgbColor(56, 78, 78));
        drawerBackground.setBackground(element);
        if (!drawerOpen) {
            drawer.setTranslationX(-mScreenWith);
        }
        drawer.setClickedListener(clickedListener);
        drawerBackground.setClickedListener(clickedListener);
    }


    private final Component.ClickedListener clickedListener = component -> {
        if (running) {
            return;
        }
        int id = component.getId();
        if (id == ResourceTable.Id_drawerBackground) {
            //点击抽屉背景，收起抽屉
            startAnimation();
        }
    };

    /**
     * 抽屉开关动画
     */
    protected void startAnimation() {
        AnimatorValue anim = new AnimatorValue();
        anim.setDuration(300);
        anim.setCurveType(Animator.CurveType.LINEAR);
        anim.setValueUpdateListener((animatorValue, v) -> {
            float tranX = drawerOpen ? (-mScreenWith * v) : (mScreenWith * v - mScreenWith);
            System.out.println("======tranX = " + tranX);
            drawerBackground.setAlpha((int) (drawerOpen ? (background_alpha - background_alpha * v) : (background_alpha * v)));
            drawer.setTranslationX(tranX);
        });
        anim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                running = true;
                if (!drawerOpen) {
                    drawer.setVisibility(Component.VISIBLE);
                    drawerBackground.setVisibility(Component.VISIBLE);
                }
            }

            @Override
            public void onStop(Animator animator) {
                running = false;
                drawerOpen = !drawerOpen;
                if (!drawerOpen) {
                    drawer.setVisibility(Component.HIDE);
                    drawerBackground.setVisibility(Component.HIDE);
                }
            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        anim.start();
    }

    private ComponentContainer createView(ComponentContainer component) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(actionBarColor));
        Image fadingActionBar = (Image) component.findComponentById(ResourceTable.Id_fadingActionBar);
        NestedScrollView scrollView = (NestedScrollView) component.findComponentById(ResourceTable.Id_scrollView);
        ComponentContainer contentLayout = (ComponentContainer) component.findComponentById(ResourceTable.Id_content);

        Text textBarText = (Text) component.findComponentById(ResourceTable.Id_textBar);
        textBarText.setText(textBar);


        if (headerResId != -1) {
            header = LayoutScatter.getInstance(component.getContext()).parse(headerResId, null, false);
            contentLayout.addComponent(header);
            scrollView.setScrolledListener((cpt, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                if (fadingActionBar.getBackgroundElement() == null) {
                    fadingActionBar.setBackground(shapeElement);
                }
                float MaxY = header.getHeight() - 150f;
                float ratio = scrollY > MaxY ? 1f : scrollY / MaxY;
                fadingActionBar.setAlpha(ratio);
            });
        }

        if (contentId != -1) {
            Component content = LayoutScatter.getInstance(component.getContext()).parse(contentId, null, false);

            if (screenHeight > 0) {
                DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(screenWidth, screenHeight);
                content.setLayoutConfig(layoutConfig);
            } else if (screenWidth > 0) {
                DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(screenWidth, 2500);
                content.setLayoutConfig(layoutConfig);
            }
            contentLayout.addComponent(content);
        }


        if (isDrawer) {
            Image drawerIcon = (Image) component.findComponentById(ResourceTable.Id_drawerIcon);
            drawerIcon.setVisibility(Component.VISIBLE);
            drawerIcon.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    openDrawer();
                }
            });
            ListContainer citiesListContainer = (ListContainer) component.findComponentById(ResourceTable.Id_cityListContainer);
            citiesListContainer.setItemProvider(provider);
            citiesListContainer.setItemClickedListener((listContainer, component1, i, l) -> {
                if (mOnDrawerItemClickListener != null) {
                    mOnDrawerItemClickListener.onDrawerItemClick(i);
                }

                switch (i) {
                    case 0:
                        textBarText.setText("New York City");
                        break;
                    case 1:
                        textBarText.setText("Tokyo");
                        break;
                    case 2:
                        textBarText.setText("London");
                        break;

                }


            });
        }


        if (isMenu == false) {
            Text menu = (Text) component.findComponentById(ResourceTable.Id_menu);
            menu.setText("");
        }


        return component;
    }

    public interface OnDrawerItemClickListener {
        void onDrawerItemClick(int position);
    }


}


